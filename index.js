// Creating trainer object properties
let trainer = {
    name: 'Ash Ketchum',
    age: 19,
    pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
    friends: {
        hoenn: ['May', 'Max'],
        kanto: ['Brock', 'Misty']
    },
    talk: function(){
        console.log('Pikachu! I choose you!');
    }
}
// log the trainer
console.log(trainer);
// log the trainer name using dot notation
console.log('Result of dot notation:');
console.log(trainer.name);
// log the pokemons using square bracket notation
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);
// invoking the function 'talk' from object properties
console.log('Result of talk method');
trainer.talk();

// Constructor for creating a Pokemon
function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    this.faint = function(opponent) {
        console.log(opponent.name + ' has been fainted');
    };
    this.tackle = function(target){
       console.log(this.name + ' tackled ' + target.name);
       target.health -= this.attack;
       console.log(target.name + "'s health is now reduce to " + target.health);
       if(target.health <= 0){
        this.faint(target);
        } 
        else if (this.health <= 0){
        console.log(this.name + ' has been fainted');
        }
    }
}

let pikachu = new Pokemon('Pikachu', 12);
console.log(pikachu);
let geodude = new Pokemon('Geodude', 8);
console.log(geodude);
let mewtwo = new Pokemon('Mewtwo', 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);